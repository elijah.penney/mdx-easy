import { Module } from 'webpack';
import { RouteComponentProps } from '@reach/router';

// Articles

export interface RelatedArticle {
  section: string,
  slug: string,
}

export interface ArticleModule extends Module {
  section: string;
  tags?: Array<string>;
  slug: string;
  name: string;
  keywords?: Array<string>;
  related?: Array<RelatedArticle>;
  [key: string]: any; // This is for key signature
}

export interface ArticleMeta {
  section: string;
  tags?: Array<string>;
  slug: string;
  name: string;
  keywords?: Array<string>;
  [key: string]: string | Array<string>; // This is for key signature
}

// Code Block
export interface CodeBlockProps {
  className: string,
}

// Pages
export interface ArticlePageProps extends RouteComponentProps {
  section: string,
  slug: string,
}
