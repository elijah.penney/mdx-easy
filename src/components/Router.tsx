import React from 'react';
import { Router as ReachRouter } from '@reach/router';
import Loading from './Loading';
import styled from 'styled-components';
import { PageLayout } from '@components';

const StyledRouter = styled(ReachRouter)`
  height: 100%;
`;

const AsyncHome = React.lazy(() => import('./Home'));

export const Router: React.FC = () => {
  return (
    <PageLayout>
      <React.Suspense fallback={<Loading />}>
        <StyledRouter>
          <AsyncHome path='/' />
        </StyledRouter>
      </React.Suspense>
    </PageLayout>
  );
};
