import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { Navbar } from '@components';
import styled from 'styled-components';

const ContentContainer = styled.div`
  max-width: 768px;
  width: 100%;
  padding: 1rem 1rem 0;
`;

const ContentSpacer = styled.div`
  display: flex;
  justify-content: space-around;
  height: 100%;
`;

export const PageLayout: React.FC<RouteComponentProps> = ({ children }) => {
  return (
    <ContentSpacer>
      <ContentContainer>
        <Navbar />
        {children}
      </ContentContainer>
    </ContentSpacer>
  );
};
