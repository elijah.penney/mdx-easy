import React from 'react';
import { RouteComponentProps } from '@reach/router';

const Loading: React.FC<RouteComponentProps> = () => {
  return (
    <div>
      Loading...
    </div>
  );
};

export default Loading;
