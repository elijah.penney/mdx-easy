import React from 'react';
import { MDXProvider } from '@mdx-js/react';


export const MarkdownProvider: React.FC = ({ children }) => {
  return (
    <MDXProvider components={{}}>
      {children}
    </MDXProvider>
  );
};
