import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html {
    margin: 0;
    padding: 0;
    font-size: 16px;
  }

  body {
    font-size: 16px;
    color: rgba(0, 0, 0, 0.7);
  }
`;
