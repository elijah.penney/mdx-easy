import React from 'react';
import { Router, GlobalStyle, Theme } from '@components';

export const App: React.FC = () => {
  return (
    <>
      <GlobalStyle />
      <Theme>
        <Router />
      </Theme>
    </>
  );
};

