import React from 'react';
import { RouteComponentProps } from '@reach/router';
import styled from 'styled-components';

const NavbarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const Spacer = styled.div`
  max-width: 40%;
  width: 100%;
`;

export const Navbar: React.FC<RouteComponentProps> = () => {
  return (
    <NavbarContainer>
      <div>
        <h1>
          LOGO
        </h1>
      </div>
      <Spacer />
      <div>
        <h1>
          SEARCH
        </h1>
      </div>
    </NavbarContainer>
  );
};

