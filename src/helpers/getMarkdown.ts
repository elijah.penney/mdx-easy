export async function getMarkdown(section: string, slug: string) {
  const markdown = await import(`../../articles/${section}/${slug}/index.mdx`);
  return markdown;
}
