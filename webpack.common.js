const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const app = require('./package.json');

/**
 * Transforms a string into a URL safe Base64 encoding. This is done to obscure the names of the libraries we use.
 * @param {String} filename - The name of the file
 * @returns {String} - A URL safe Base64 encoded filename
 */
function urlSafeBase64(filename) {
  let base = Buffer.from(filename.replace('@', '')).toString('base64');
  base = base.replace(/\//g, '_');
  base = base.replace(/\+/g, '-');
  base = base.replace(/=/g, '');

  return base;
}

module.exports = {
  entry: './src/index.tsx',
  output: {
    filename: '[name].js',
    chunkFilename: `[name].v${app.version}.chunk.js`,
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './',
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]; // eslint-disable-line

            // npm package names are URL-safe, but some servers don't like @ symbols
            return urlSafeBase64(packageName);
          },
        },
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.less$/, use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'less-loader',
          options: {
            javascriptEnabled: true,
          },
        }],
      },
      {
        test: /.js(x?)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          plugins: ['syntax-dynamic-import'],
        },
      },
      {
        test: /.ts(x?)$/,
        use: ['babel-loader', 'ts-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.md(x?)$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: ['@babel/plugin-transform-react-jsx'],
            },
          },
          {
            loader: '@mdx-js/loader',
          },
        ],
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
    plugins: [
      new TsconfigPathsPlugin(),
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      favicon: 'public/favicon.ico',
    }),
  ],
};
